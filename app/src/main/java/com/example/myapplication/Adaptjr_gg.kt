package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import java.time.Year

class Adaptjr_gg (val info: List<String>) : RecyclerView.Adapter<Adaptjr_gg.ViewHolder>() {
    class ViewHolder (v : View): RecyclerView.ViewHolder (v)  {
        val view = v.findViewById<TextView>(R.id.gg)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rr : View = LayoutInflater.from(parent.context).inflate(R.layout.gg,parent,false)
        return ViewHolder(rr)
    }

    override fun getItemCount(): Int =info.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.text = info[position]
    }
}